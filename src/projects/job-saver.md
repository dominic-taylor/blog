---
layout: default
title: JobSaver for LinkedIn
---

I made a browser extension to help track job applications on LinkedIn.

<!-- excerpt -->

Apparently I need to have a page somewhere on the internet to publish it to the Chrome store, so here we are.
<br>
<br>
Essentially it lets you save job posts as a CSV so you can open it in Excel or Sheets and track your job applications. 
<br>
<br>

Key Features:
<ul class="list-disc list-inside">
<li class="ml-2">Save Job Posts: Conveniently save job postings directly from LinkedIn with a single click.</li>
<li class="ml-2">Export to CSV: Seamlessly export your saved job posts to a CSV file for easy access and management.</li>
<li class="ml-2">Privacy Protection: Rest assured that your data remains private and secure, as the extension only accesses and saves job post information to your device without compromising your LinkedIn account.</li>
</ul>
<br>
I found it pretty helpful in my job hunting. Here's the link to the Chrome store page if you want to try it.