---
layout: default
title: Hello.
---

Hi, I'm Dominic, a front end and web developer.   
  
<p class="py-4">
I'm currently working remotely from the the EU, and am interested in working with front ends of all kinds, but particularly React, Vue, Next.js or similar. I also have extensive experience site building with Drupal and WordPress. In my spare time I enjoy the sea, philosophy, the <i>arts</i>, culture and <b>rugby</b>.   
</p>  
<p>I am open to work right now, so if you would like to contact me, feel free to email me through <a class="underline" href="mailto:hello@virtualdom.net">here</a>.</p>