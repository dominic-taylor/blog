---
layout: default
title: Projects
---

A list of my projects from the last while. The include personal and commercial ideas, code and not-code businesses and apps. 
  
<div class="py-4">
<h2 class="font-bold">JobSaver for LinkedIn</h2>
<p class="py-4">
A little browser extension that makes job hunting on LinkedIn a bit more bearable. 
<br>
<br>
When on the job search page of LinkedIn, it adds 2 buttons that let you save job listings to a list and download it as a CSV. You can then open it in Excel or Sheets and track your applications. Currently only available for Chrome but I plan on converting for Firefox asap.
<br>
<br>
<a class="underline" href="#">Link to Chrome Web Store listing</a>
</p>
</div>

<div class="py-4">
<h2 class="font-bold">Standard Drink Calculator</h2>
<p class="py-4">
This is a little personal project of mine which I use quite often. In some countries, there is a standard formula for calculating the number of drinks in a measure of alcohol, each "standard drink" being roughly equivalant to one shot, one bottle of beer or a glass of wine. This is printed on the label and I use it often when I'm at home. But when I was traveling in Europe I noticed it wasn't in some countries, so I made a online calculator to always have it available. It's useful for knowing how many drinks you should buy to serve at a party or take for a weekend trip.
<br>
<br>
<a class="underline" href="https://standarddrinkcalculator.com/">standarddrinkcalculator.com</a>
</p>
</div>

<div class="py-4">
<h2 class="font-bold">NZ Scratch Posters</h2>
<p class="py-4">
Some friends and I have been working on this idea for a couple years. The concept is a poster of the most common fish species in New Zealand, each covered by a little square of foil (like a lottery ticket), which the fisher scratches of when they catch them (or claim too). We have the posters printed and sell them from an online store and in person. 
<br>
<br>
<a class="underline" href="https://nzscratchposters.co.nz/">NZ Scratch Posters</a>
</p>  
<p>I am open to work right now, so if you would like to contact me, feel free to email me through <a class="underline" href="mailto:hello@virtualdom.net">here</a>.</p>