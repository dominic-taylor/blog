---
  title: 'JobSaver for LinkedIn'
  date: 2024-02-01
  tags:
    - app
    - browser-extension
    - chrome
    - firefox
---

I made a browser extension to help track job applications on LinkedIn.

<!-- excerpt -->

Apparently I need to have a page somewhere on the internet to publish it to the Chrome store, so here we are.
<br>
<br>
Essentially it lets you save job posts as a CSV so you can open it in Excel or Sheets and track your job applications. 
<br>

Key Features:

- Save Job Posts: Conveniently save job postings directly from LinkedIn with a single click.
- Export to CSV: Seamlessly export your saved job posts to a CSV file for easy access and management.
- Privacy Protection: Rest assured that your data remains private and secure, as the extension only accesses and saves job post information to your device without compromising your LinkedIn account.